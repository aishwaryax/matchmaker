using Microsoft.EntityFrameworkCore;
using MatchMaker.API.Models;

namespace MatchMaker.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options) {}
        public DbSet<Value> Values { get; set; }
    }
}